## on5ltetmo-user 6.0.1 MMB29K G550TUVU2AQC4 release-keys
- Manufacturer: samsung
- Platform: 
- Codename: on5ltetmo
- Brand: samsung
- Flavor: lineage_on5ltetmo-userdebug
- Release Version: 10
- Id: QQ3A.200805.001
- Incremental: eng.jack.20211025.175304
- Tags: test-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: samsung/on5ltetmo/on5ltetmo:6.0.1/MMB29K/G550TUVU2AQC4:user/release-keys
- OTA version: 
- Branch: on5ltetmo-user-6.0.1-MMB29K-G550TUVU2AQC4-release-keys
- Repo: samsung_on5ltetmo_dump_21526


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
