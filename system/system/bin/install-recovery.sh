#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:12648464:829b5c5c02b00e46e8d5fcd58b15b7c6e5946429; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/13540000.dwmmc0/by-name/BOOT:6557712:190c9205e49121f0065bfbd9a71a97966768c263 \
          --target EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:12648464:829b5c5c02b00e46e8d5fcd58b15b7c6e5946429 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
